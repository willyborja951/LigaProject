from administracion import models
from django import forms
from django.core.exceptions import ObjectDoesNotExist
import datetime

class validators():
    def validate_file_extension(value):
        if not value.name.endswith('.csv'):
            raise forms.ValidationError("Se admite únicamente archivos en formato .CSV")

#Formulario de login
class LoginForm(forms.Form):
    username = forms.CharField(label='Username',required=True, widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(label='Password',required=True, widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))

#Formulario crear nuevo Catalogo
class AgregarCatalogoForm(forms.ModelForm):
    nombreCatalogo = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    descripcionCatalogo = forms.CharField(label='Descripción', required=True, widget=forms.TextInput(attrs={'placeholder': 'Descripción'}))

    class Meta:
        model = models.Catalogo
        fields = ('nombreCatalogo', 'descripcionCatalogo',)

#Formulario actualizar un Catalogo
class ActualizarCatalogoForm(forms.Form):
    id = forms.IntegerField(required=True)
    nombreCatalogo = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    descripcionCatalogo = forms.CharField(label='Descripción', required=True, widget=forms.TextInput(attrs={'placeholder': 'Descripción'}))

    def clean(self):
        id = self.cleaned_data.get('id')
        nombreCatalogo = self.cleaned_data.get('nombreCatalogo')
        try:
            catalogo = models.Catalogo.objects.get(nombreCatalogo=nombreCatalogo)
            if catalogo.id != id:
                raise forms.ValidationError({'nombreCatalogo':["El nombre del catalogo ingresado ya se encuentra registrado. Por favor, ingresa uno nuevo."]})
        except ObjectDoesNotExist:
            pass

#Formulario crear nuevo Item Catalogo
class AgregarItemCatalogoForm(forms.ModelForm):
    nombreItem = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    catalogo = forms.ModelChoiceField(queryset=models.Catalogo.objects.all(), empty_label='Seleccionar')

    class Meta:
        model = models.ItemCatalogo
        fields = ('nombreItem', 'catalogo',)

    def clean(self):
        nombreItem = self.cleaned_data.get('nombreItem')
        catalogo = self.cleaned_data.get('catalogo')
        try:
            itemCatalogo = models.ItemCatalogo.objects.get(nombreItem=nombreItem, catalogo=catalogo)
            raise forms.ValidationError({'nombreItem':["El nombre del item de catálogo ingresado ya ha sido asignado al catálogo seleccionado. Por favor, ingrese otro nombre."]})
        except ObjectDoesNotExist:
            pass

#Formulario actualizar un Item Catalogo
class ActualizarItemCatalogoForm(forms.Form):
    id = forms.IntegerField(required=True)
    nombreItem = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    catalogo = forms.ModelChoiceField(queryset=models.Catalogo.objects.all(), empty_label='Seleccionar')

    def clean(self):
        id = self.cleaned_data.get('id')
        nombreItem = self.cleaned_data.get('nombreItem')
        catalogo = self.cleaned_data.get('catalogo')
        try:
            itemCatalogo = models.ItemCatalogo.objects.get(nombreItem=nombreItem, catalogo=catalogo)
            if itemCatalogo.id != id:
                raise forms.ValidationError({'nombreItem':["El nombre del item de catálogo ingresado ya ha sido asignado al catálogo seleccionado. Por favor, ingrese otro nombre."]})
        except ObjectDoesNotExist:
            pass

#Formulario crear nuevos usuarios
class CredencialForm(forms.ModelForm):
    name = forms.CharField(label='Nombres', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombres'}))
    lastname = forms.CharField(label='Apellidos',required=True, widget=forms.TextInput(attrs={'placeholder': 'Apellidos'}))
    username = forms.CharField(label='Username',required=True, widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    email = forms.EmailField(label='Email',required=True, widget=forms.TextInput(attrs={'placeholder': 'Email'}))
    tipoUsuario = forms.ModelChoiceField(label='Tipo de usuario',queryset= models.ItemCatalogo.objects.all(), empty_label="Seleccionar")
    password = forms.CharField(label='Password',required=True, widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    
    class Meta:
        model = models.Credencial
        fields = ('name','lastname','username', 'email', 'password', 'tipoUsuario')

#Formulario crear nuevo equipo
class AgregarEquipoForm(forms.ModelForm):
    nombre = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    modalidad = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="Modalidad de campeonato")), empty_label='Seleccionar')

    class Meta:
        model = models.Equipo
        fields = ('nombre', 'modalidad',)

    def clean(self):
        nombre = self.cleaned_data.get('nombre')
        modalidad = self.cleaned_data.get('modalidad')
        try:
            equipo = models.Equipo.objects.get(nombre=nombre, modalidad=modalidad)
            raise forms.ValidationError({'nombre':["El nombre de equipo ingresado ya se encuentra registrado. Por favor, ingresa uno nuevo."]})
        except ObjectDoesNotExist:
            pass

#Formulario actualizar un equipo
class ActualizarEquipoForm(forms.Form):
    id = forms.IntegerField(required=True)
    nombre = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    modalidad = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="Modalidad de campeonato")), empty_label='Seleccionar')

    def clean(self):
        id = self.cleaned_data.get('id')
        nombre = self.cleaned_data.get('nombre')
        modalidad = self.cleaned_data.get('modalidad')
        try:
            equipo = models.Equipo.objects.get(nombre=nombre, modalidad=modalidad)
            if equipo.id != id:
                raise forms.ValidationError({'nombre':["El nombre de equipo ingresado ya se encuentra registrado. Por favor, ingresa uno nuevo."]})
        except ObjectDoesNotExist:
            pass

"""
CAMPEONATOS DEPORTIVOS
"""
#Crea un nuevo Campeonato Deportivo
class AgregarCampeonatoDeportivoForm(forms.ModelForm):
    nombre = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    fechaInicio = forms.DateField(label="Fecha de inicio", required=True)
    predefinido = forms.BooleanField(initial=False, required=False, label='predefinido')

    class Meta:
        model = models.CampeonatoDeportivo
        fields = ('nombre', 'fechaInicio', 'predefinido',)

#Actualiza un campeonato Deportivo
class ActualizarCampeonatoDeportivoForm(forms.Form):
    id = forms.IntegerField(required=True)
    nombre = forms.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    fechaInicio = forms.DateField(label="Fecha de inicio", required=True)
    predefinido = forms.BooleanField(initial=False, required=False, label='predefinido')

    def clean(self):
        id = self.cleaned_data.get('id')
        nombre = self.cleaned_data.get('nombre')
        fechaInicio = self.cleaned_data.get('fechaInicio')
        try:
            campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(nombre=nombre)
            if campeonatoDeportivo.id != id:
                raise forms.ValidationError({'nombre':["El nombre del campeonato ingresado ya se encuentra registrado. Por favor, ingresa uno nuevo."]})
        except ObjectDoesNotExist:
            pass

        try:
            campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(fechaInicio=fechaInicio)
            if campeonatoDeportivo.id != id:
                raise forms.ValidationError({'fechaInicio':["La fecha de inicio ingresada ya se encuentra registrada. Por favor, ingresa una nueva."]})
        except ObjectDoesNotExist:
            pass

"""
SUBCAMPEONATO
"""
#Crea un nuevo Campeonato Deportivo
class AgregarSubcampeonatoForm(forms.ModelForm):
    campeonatoDeportivo = forms.ModelChoiceField(queryset=models.CampeonatoDeportivo.objects.all(), empty_label='Seleccionar')
    modalidad = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="Modalidad de campeonato")), empty_label='Seleccionar')

    class Meta:
        model = models.Subcampeonato
        fields = ('campeonatoDeportivo', 'modalidad',)

    def clean(self):
        id = self.cleaned_data.get('id')
        campeonatoDeportivo = self.cleaned_data.get('campeonatoDeportivo')
        modalidad = self.cleaned_data.get('modalidad')
        try:
            subcampeonato = models.Subcampeonato.objects.get(campeonatoDeportivo=campeonatoDeportivo, modalidad=modalidad)
            if subcampeonato.id != id:
                raise forms.ValidationError({'campeonatoDeportivo':["Ya existe una asignación campeonato - modalidad igual a la ingresada."]})
        except ObjectDoesNotExist:
            pass

#Actualiza un campeonato Deportivo
class ActualizarSubcampeonatoForm(forms.Form):
    id = forms.IntegerField(required=True)
    campeonatoDeportivo = forms.ModelChoiceField(queryset=models.CampeonatoDeportivo.objects.all(), empty_label='Seleccionar')
    modalidad = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="Modalidad de campeonato")), empty_label='Seleccionar')

    def clean(self):
        id = self.cleaned_data.get('id')
        campeonatoDeportivo = self.cleaned_data.get('campeonatoDeportivo')
        modalidad = self.cleaned_data.get('modalidad')
        try:
            subcampeonato = models.Subcampeonato.objects.get(campeonatoDeportivo=campeonatoDeportivo, modalidad=modalidad)
            if subcampeonato.id != id:
                raise forms.ValidationError({'campeonatoDeportivo':["Ya existe una asignación campeonato - modalidad igual a la ingresada."]})
        except ObjectDoesNotExist:
            pass

"""
INSCRIPCION EQUIPO
"""
#Crea un nuevo Campeonato Deportivo
class AgregarInscripcionEquipoForm(forms.ModelForm):
    equipo = forms.ModelChoiceField(queryset=models.Equipo.objects.all(), empty_label='Seleccionar')
    subcampeonato = forms.ModelChoiceField(queryset=models.Subcampeonato.objects.all(), empty_label='Seleccionar')
    class Meta:
        model = models.InscripcionEquipo
        fields = ('equipo', 'subcampeonato',)
        
    def clean(self):
        equipo = self.cleaned_data.get('equipo')
        subcampeonato = self.cleaned_data.get('subcampeonato')
        try:
            inscripcionEquipo = models.InscripcionEquipo.objects.get(equipo=equipo, subcampeonato=subcampeonato)
            raise forms.ValidationError({'equipo':["Ya existe una asignación equipo - subcampeonato igual a la ingresada."]})
        except ObjectDoesNotExist:
            pass

#Actualiza un campeonato Deportivo
class ActualizarInscripcionEquipoForm(forms.Form):
    id = forms.IntegerField(required=True)
    equipo = forms.ModelChoiceField(queryset=models.Equipo.objects.all(), empty_label='Seleccionar')
    subcampeonato = forms.ModelChoiceField(queryset=models.Subcampeonato.objects.all(), empty_label='Seleccionar')

    def clean(self):
        id = self.cleaned_data.get('id')
        equipo = self.cleaned_data.get('equipo')
        subcampeonato = self.cleaned_data.get('subcampeonato')
        try:
            inscripcionEquipo = models.InscripcionEquipo.objects.get(equipo=equipo, subcampeonato=subcampeonato)
            if inscripcionEquipo.id != id:
                raise forms.ValidationError({'equipo':["Ya existe una asignación equipo - subcampeonato igual a la ingresada."]})
        except ObjectDoesNotExist:
            pass

"""
JUGADOR
"""
#Crea un nuevo jugador
class AgregarJugadorForm(forms.ModelForm):
    nombres = forms.CharField(label='Nombres', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombres'}))
    apellidos = forms.CharField(label='Apellidos', required=True, widget=forms.TextInput(attrs={'placeholder': 'Apellidos'}))
    numeroCedula = forms.CharField(label='Numero Cédula', required=True, widget=forms.TextInput(attrs={'placeholder': 'Número Cédula'}), max_length=models.Jugador._meta.get_field('numeroCedula').max_length)
    genero = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="genero de jugadores")), empty_label='Seleccionar')

    class Meta:
        model = models.Jugador
        fields = ('nombres', 'apellidos','numeroCedula',)

#Actualiza un jugador
class ActualizarJugadorForm(forms.Form):
    id = forms.IntegerField(required=True)
    nombres = forms.CharField(label='Nombres', required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombres'}))
    apellidos = forms.CharField(label='Apellidos', required=True, widget=forms.TextInput(attrs={'placeholder': 'Apellidos'}))
    numeroCedula = forms.CharField(label='Numero Cédula', required=True, widget=forms.TextInput(attrs={'placeholder': 'Número Cédula'}), max_length=models.Jugador._meta.get_field('numeroCedula').max_length)
    genero = forms.ModelChoiceField(queryset=models.ItemCatalogo.objects.filter(catalogo=models.Catalogo.objects.get(nombreCatalogo="genero de jugadores")), empty_label='Seleccionar')

    def clean(self):
        id = self.cleaned_data.get('id')
        numeroCedula = self.cleaned_data.get('numeroCedula')
        try:
            jugador = models.Jugador.objects.get(numeroCedula=numeroCedula)
            if jugador.id != id:
                raise forms.ValidationError({'numeroCedula':["El número de cédula ingresado ya se encuentra registrado. Por favor, ingresa uno nuevo."]})
        except ObjectDoesNotExist:
            pass

"""
PASE DEPORTIVO
"""
#Crea un nuevo pase deportivo
class AgregarPaseDeportivoForm(forms.ModelForm):
    jugador = forms.ModelChoiceField(queryset=models.Jugador.objects.all(), empty_label='Seleccionar')
    equipoDestino = forms.ModelChoiceField(queryset=models.Equipo.objects.all(), empty_label='Seleccionar')

    class Meta:
        model = models.Jugador
        fields = ('jugador', 'equipoDestino',)

    def clean(self):
        jugador = self.cleaned_data.get('jugador')
        equipoDestino = self.cleaned_data.get('equipoDestino')
        campeonatoDeportivoVigencia = models.CampeonatoDeportivo.objects.get(predefinido=True)
        try:
            paseDeportivo = models.PaseDeportivo.objects.get(jugador=jugador, campeonatoDeportivoVigencia=campeonatoDeportivoVigencia)
            raise forms.ValidationError({'jugador':["Ya existe un pase deportivo generado para el jugador especificado en el actual campeonato deportivo; para generar uno nuevo, elimine el pase deportivo actual del jugador."]})
        except ObjectDoesNotExist:
            pass

"""
INSCRIPCION JUGADOR
"""
#Crea un nuevo Campeonato Deportivo
class AgregarInscripcionJugadorForm(forms.ModelForm):
    inscripcionEquipo = forms.ModelChoiceField(queryset=models.InscripcionEquipo.objects.all(), empty_label='Seleccionar')
    jugador = forms.ModelChoiceField(queryset=models.Jugador.objects.all(), empty_label='Seleccionar')
    class Meta:
        model = models.InscripcionJugador
        fields = ('inscripcionEquipo', 'jugador',)
    """
    def clean(self):
        inscripcionEquipo = self.cleaned_data.get('inscripcionEquipo')
        jugador = self.cleaned_data.get('jugador')
        try:
            inscripcionEquipo = models.InscripcionEquipo.objects.get(equipo=equipo, subcampeonato=subcampeonato)
            raise forms.ValidationError({'equipo':["Ya existe una asignación equipo - subcampeonato igual a la ingresada."]})
        except ObjectDoesNotExist:
            pass
    """

class FechaDeportivaForm(forms.ModelForm):
    nombre = forms.CharField(label= 'Nombre de la fecha deportiva',required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre de la fecha deportiva'}))

    class Meta:
        model = models.FechaDeportiva
        fields = ('nombre',)

class ActualizarFechaDeportivaForm(forms.Form):
    idNombre = forms.IntegerField(required=True)
    nombre= forms.CharField(label='Nombre de la fecha deportiva',required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombre de la fecha deportiva'}))

#Formulario para subir la lista de equipos y jugadores, formato csv
class ListaEquipos(forms.ModelForm):
    file = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}), required=True, validators=[validators.validate_file_extension])
    
    class Meta:
        model = models.ListaEquipos
        fields = ('file',)

class FechaForm(forms.ModelForm):
    fechaDeportiva= forms.ModelChoiceField(queryset=models.FechaDeportiva.objects.all(), empty_label='Select the category')
    fecha = forms.DateField(label="Fecha de participación", required=True)

    class Meta:
        model = models.Fecha
        fields = ('fechaDeportiva','fecha',)

class ActualizarFechaForm(forms.ModelForm):
    idFecha = forms.IntegerField(required=True)
    fechaDeportiva= forms.ModelChoiceField(queryset=models.FechaDeportiva.objects.all(), empty_label='Select the category')
    fecha = forms.DateField(label="Fecha de participación", required=True)

    class Meta:
        model = models.Fecha
        fields = ('fechaDeportiva','fecha',)

class EncuentroDeportivoForm(forms.ModelForm):
    fecha = forms.ModelChoiceField(queryset=models.Fecha.objects.all(), empty_label='Select the category')
    hora = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
    equipo1 = forms.ModelChoiceField(queryset=models.Equipo.objects.all(), empty_label='Select the category')
    equipo2 = forms.ModelChoiceField(queryset=models.Equipo.objects.all(), empty_label='Select the category')

    class Meta:
        model = models.EncuentroDeportivo
        fields = ('fecha','hora','equipo1','equipo2',)

class ArrayEncuentroDeportivoForm(forms.Form):
    encuentroDeportivo = EncuentroDeportivoForm()

class HorarioForm(forms.ModelForm):
    nombre = forms.CharField(label='Nombre del horario',required=True)

    class Meta:
        model = models.Horario
        fields = ('nombre',)

class ActualizarHorarioForm(forms.ModelForm):
    idHorario = forms.IntegerField(required=True)
    nombre = forms.CharField(label='Nombre del horario',required=True)

    class Meta:
        model = models.Horario
        fields = ('idHorario','nombre',)

class HoraForm(forms.ModelForm):
    horario = forms.ModelChoiceField(queryset=models.Horario.objects.all(), empty_label='Select the category')
    hora = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))

    class Meta:
        model = models.Horario
        fields = ('horario','hora',)

class ActualizarHoraForm(forms.ModelForm):
    idHora = forms.IntegerField(required=True)
    horario = forms.ModelChoiceField(queryset=models.Horario.objects.all(), empty_label='Select the category')
    hora = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))

    class Meta:
        model = models.Horario
        fields = ('idHora','horario','hora',)