from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils import timezone
from django.db import models
from Liga import settings
from django.dispatch import receiver
import os

"""
CATALOGO
"""
class Catalogo(models.Model):
    nombreCatalogo = models.CharField(max_length=50, unique=True, null=False)
    descripcionCatalogo = models.CharField(max_length=50, unique=False, null=True)

    def __str__(self):
        return str(self.nombreCatalogo)

"""
ITEM CATALOGO
"""
class ItemCatalogo(models.Model):
    nombreItem = models.CharField(max_length=50, null=False)
    catalogo = models.ForeignKey(Catalogo, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.nombreItem)

"""
ADMINISTRADOR DE CREDENCIALES
"""
class CredencialManager(BaseUserManager):
    def create_user(self, name, lastname, username, email, password):
        credencial = self.model(name = name, lastname = lastname, username = username)
        email = self.normalize_email(email)
        credencial.set_password(password)
        credencial.save(using=self._db)
        return credencial

    def get_by_natural_key(self, username):
        return self.get(username=username)

"""
CREDENCIAL
"""
class Credencial(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=15, unique=True, null=False)
    name = models.CharField(max_length=100, unique=False, null=False)
    lastname = models.CharField(max_length=100, unique=False, null=False)
    tipoUsuario = models.ForeignKey(ItemCatalogo, on_delete=models.CASCADE)
    email = models.EmailField(max_length=100, unique=True, null=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    last_modified = models.DateTimeField(default=timezone.now)
    date_joined = models.DateTimeField(default=timezone.now)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = ('Credencial')
        verbose_name_plural = ('Credenciales')
        
    objects = CredencialManager()

    def get_full_name(self):
        return self.name+" "+self.lastname
    def get_short_name(self):
        return self.name
    def __unicode__(self):
        return self.email
    def has_perm(self, perm, obj=None):
        return True
    def has_module_perms(self, app_label):
        return True
    
    @property
    def is_staff(self):
        return self.is_admin

"""
CAMPEONATO DEPORTIVO
"""
class CampeonatoDeportivo(models.Model):
    nombre = models.CharField(max_length=50, unique=True, null=False)
    fechaInicio = models.DateField(null=False, unique=True)
    predefinido = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return str(self.nombre)

"""
EQUIPO
"""
class Equipo(models.Model):
    nombre = models.CharField(max_length=50, null=False)
    modalidad = models.ForeignKey(ItemCatalogo, on_delete=models.CASCADE) #Masculino/femenino
    def __str__(self):
        return str(self.nombre)

"""
SUBCAMPEONATO
"""
class Subcampeonato(models.Model):
    campeonatoDeportivo = models.ForeignKey(CampeonatoDeportivo, on_delete=models.CASCADE)
    modalidad = models.ForeignKey(ItemCatalogo, on_delete=models.CASCADE) #Masculino/femenino
    def __str__(self):
        return str(self.modalidad)

"""
INSCRIPCIÓN DEL EQUIPO DEPORTIVO
"""
class InscripcionEquipo(models.Model):
    subcampeonato = models.ForeignKey(Subcampeonato, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)

    def __str__(self):
        return str(str(self.subcampeonato) +" | "+str(self.equipo))

"""
JUGADOR
"""
class Jugador(models.Model):
    numeroCedula = models.CharField(max_length=10, null=False, unique=True)
    nombres = models.CharField(max_length=50, null=False)
    apellidos = models.CharField(max_length=50, null=False)
    genero = models.ForeignKey(ItemCatalogo, on_delete=models.CASCADE) 
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE, null=True) 

    def __str__(self):
        return str(self.nombres+" "+self.apellidos)

"""
INSCRIPCIÓN JUGADOR
"""
class InscripcionJugador(models.Model):
    jugador = models.ForeignKey(Jugador, on_delete=models.CASCADE)
    inscripcionEquipo = models.ForeignKey(InscripcionEquipo, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.jugador +", "+self.inscripcionEquipo)

"""
PASE DEPORTIVO
"""
class PaseDeportivo(models.Model):
    jugador = models.ForeignKey(Jugador, on_delete=models.CASCADE)
    equipoDestino = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    campeonatoDeportivoVigencia = models.ForeignKey(CampeonatoDeportivo, on_delete=models.CASCADE)


    def __str__(self):
        return str(self.jugador +", "+ self.equipoDestino +", "+ self.campeonatoDeportivoVigencia)

"""
FECHA DEPORTIVA
"""
class FechaDeportiva(models.Model):
    nombre = models.CharField(max_length=50, null=False)
    predefinido = models.BooleanField(default=True, null=False)
    campeonatoDeportivo = models.ForeignKey(CampeonatoDeportivo, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.nombre +", "+ self.campeonatoDeportivo)

"""
FECHA
"""
class Fecha(models.Model):
    fechaDeportiva = models.ForeignKey(FechaDeportiva, on_delete=models.CASCADE)
    fecha = models.DateField(null=False, unique=True)

    def __str__(self):
        return str(self.fecha)

"""
ENCUENTRO DEPORTIVO
"""
class EncuentroDeportivo(models.Model):
    fecha = models.ForeignKey(Fecha, on_delete=models.CASCADE)
    hora = models.TimeField(null=False, blank=False)
    equipo1 = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='equipo1')
    equipo2 = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name='equipo2')

    def __str__(self):
        return str(self.fecha+" "+self.hora+" "+self.equipo1+" "+self.equipo2)

"""
REGISTRO DEPORTIVO
"""
class RegistroDeportivo(models.Model):
    anotacion = models.ForeignKey(ItemCatalogo, on_delete=models.CASCADE) #Tipo de anotacion (tarjeta amarilla, roja, goles)
    cantidad = models.IntegerField(null=True)
    encuentroDeportivo = models.ForeignKey(EncuentroDeportivo, on_delete=models.CASCADE)
    inscripcionJugador = models.ForeignKey(InscripcionJugador, on_delete=models.CASCADE)

"""
LISTA DE EQUIPOS INGRESADOS
"""
class ListaEquipos(models.Model):
    file = models.FileField(upload_to=settings.MEDIA_URL)

#Una vez procesado el archivo, éste es eliminado
@receiver(models.signals.post_delete, sender=ListaEquipos)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

"""
JUGADOR QUE NO HA SIDO CALIFICADO EN EL CAMPEONATO
"""
class JugadorFallido(models.Model):
    nombres = models.CharField(max_length=50, null=True)
    apellidos = models.CharField(max_length=50, null=True)
    numeroCedula = models.CharField(max_length=10, null=True)
    numeroCarnet = models.IntegerField(null=True)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE, null=True)
    observacion = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.nombres)

"""
NOMBRE DE HORARIO DE JUEGOS
"""
class Horario(models.Model):
    nombre = models.CharField(max_length=50, null=False, unique=True)

    def __str__(self):
        return str(self.nombre)

"""
HORAS DE CADA HORARIO DE JUEGO
"""
class Hora(models.Model):
    horario = models.ForeignKey(Horario, on_delete=models.CASCADE)
    hora = models.TimeField(null=False, blank=False)

    def __str__(self):
        return str(self.hora)

