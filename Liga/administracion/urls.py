from django.conf.urls import url, include
from administracion import views

urlpatterns=[
    #FRONTPAGE
    url(r'^$', views.index, name="index" ),
    url(r'^consultas/$', views.consultas, name="consultas" ),
    
    #ADMIN DASHBOARD
    url(r'^login/$', views.login, name="login" ),
    url(r'^cerrarSesion/$', views.cerrarSesion, name="cerrarSesion" ),
    url(r'^registrarUsuario/$', views.registrarUsuario, name="registrarUsuario" ),
    url(r'^dashboard/$', views.dashboard, name="dashboard" ),

    url(r'^agregarCatalogo/$', views.agregarCatalogo, name="agregarCatalogo" ),
    url(r'^actualizarCatalogo/$', views.actualizarCatalogo, name="actualizarCatalogo" ),
    url(r'^eliminarCatalogo/(?P<id>\d+)/$', views.eliminarCatalogo, name="eliminarCatalogo" ),

    url(r'^agregarItemCatalogo/$', views.agregarItemCatalogo, name="agregarItemCatalogo" ),
    url(r'^actualizarItemCatalogo/$', views.actualizarItemCatalogo, name="actualizarItemCatalogo" ),
    url(r'^eliminarItemCatalogo/(?P<id>\d+)/$', views.eliminarItemCatalogo, name="eliminarItemCatalogo" ),

    url(r'^agregarEquipo/$', views.agregarEquipo, name="agregarEquipo" ),
    url(r'^actualizarEquipo/$', views.actualizarEquipo, name="actualizarEquipo" ),
    url(r'^eliminarEquipo/(?P<id>\d+)/$', views.eliminarEquipo, name="eliminarEquipo" ),
    
    url(r'^agregarCampeonatoDeportivo/$', views.agregarCampeonatoDeportivo, name="agregarCampeonatoDeportivo" ),
    url(r'^actualizarCampeonatoDeportivo/$', views.actualizarCampeonatoDeportivo, name="actualizarCampeonatoDeportivo" ),
    url(r'^eliminarCampeonatoDeportivo/(?P<id>\d+)/$', views.eliminarCampeonatoDeportivo, name="eliminarCampeonatoDeportivo" ),

    url(r'^agregarSubcampeonato/$', views.agregarSubcampeonato, name="agregarSubcampeonato" ),
    url(r'^actualizarSubcampeonato/$', views.actualizarSubcampeonato, name="actualizarSubcampeonato" ),
    url(r'^eliminarSubcampeonato/(?P<id>\d+)/$', views.eliminarSubcampeonato, name="eliminarSubcampeonato" ),

    url(r'^agregarInscripcionEquipo/$', views.agregarInscripcionEquipo, name="agregarInscripcionEquipo" ),
    url(r'^eliminarInscripcionEquipo/(?P<id>\d+)/$', views.eliminarInscripcionEquipo, name="eliminarInscripcionEquipo" ),
    url(r'^consultaEquiposSubcampeonato/$', views.consultaEquiposSubcampeonato, name="consultaEquiposSubcampeonato" ), #consulta los equipos, dado un subcampeonato (masculino/femenino)

    url(r'^agregarJugador/$', views.agregarJugador, name="agregarJugador" ),
    url(r'^actualizarJugador/$', views.actualizarJugador, name="actualizarJugador" ),
    url(r'^eliminarJugador/(?P<id>\d+)/$', views.eliminarJugador, name="eliminarJugador" ),

    url(r'^agregarPaseDeportivo/$', views.agregarPaseDeportivo, name="agregarPaseDeportivo" ),
    url(r'^eliminarPaseDeportivo/(?P<id>\d+)/$', views.eliminarPaseDeportivo, name="eliminarPaseDeportivo" ),
    url(r'^consultarEquiposInscritosEnCampeonatoDeportivo/$', views.consultarEquiposInscritosEnCampeonatoDeportivo, name="consultarEquiposInscritosEnCampeonatoDeportivo" ), #retorna los equipos deportivos inscritos en un determinados campeonato deportivo
    url(r'^consultaSubcampeonatosActuales/$', views.consultaSubcampeonatosActuales, name="consultaSubcampeonatosActuales" ), #retorna los campeonatos deportivos actuales

    url(r'^agregarInscripcionJugador/$', views.agregarInscripcionJugador, name="agregarInscripcionJugador" ), 
    url(r'^eliminarInscripcionJugador/$', views.eliminarInscripcionJugador, name="eliminarInscripcionJugador" ), 
    url(r'^consultarEquiposInscritosDadoSubcampeonato/$', views.consultarEquiposInscritosDadoSubcampeonato, name="consultarEquiposInscritosDadoSubcampeonato" ),  #retorna los equipos inscritos dentro de un subcampeonato, dado el subcampeonato
    url(r'^consultarJugadoresDadoSubcampeonato/$', views.consultarJugadoresDadoSubcampeonato, name="consultarJugadoresDadoSubcampeonato" ),  #retorna los jugadores dependeindo del subcampeonato seleccionado




    url(r'^agregarEncuentrosDeportivos/$', views.agregarEncuentrosDeportivos, name="agregarEncuentrosDeportivos" ),



    url(r'^subirListado/$', views.subirListado, name="subirListado" ),
    url(r'^eliminarFechaDeportiva/(?P<idFecha>\d+)/$', views.eliminarFechaDeportiva, name="eliminarFechaDeportiva" ),
    url(r'^actualizarFechaDeportiva/$', views.actualizarFechaDeportiva, name="actualizarFechaDeportiva" ),
    url(r'^administrarFechasDeportivas/$', views.administrarFechasDeportivas, name="administrarFechasDeportivas" ),
    url(r'^administrarFechas/$', views.administrarFechas, name="administrarFechas" ),
    
    url(r'^eliminarFecha/(?P<idFecha>\d+)/$', views.eliminarFecha, name="eliminarFecha" ),
    url(r'^actualizarFecha/$', views.actualizarFecha, name="actualizarFecha" ),
    url(r'^administrarHorarios/$', views.administrarHorarios, name="administrarHorarios" ),
    url(r'^actualizarHorario/$', views.actualizarHorario, name="actualizarHorario" ),
    url(r'^eliminarHorario/(?P<idHorario>\d+)/$', views.eliminarHorario, name="eliminarHorario" ),
    url(r'^administrarHoras/$', views.administrarHoras, name="administrarHoras" ),
    url(r'^actualizarHora/$', views.actualizarHora, name="actualizarHora" ),    
    url(r'^eliminarHora/(?P<idHora>\d+)/$', views.eliminarHora, name="eliminarHora" ),
    url(r'^consultaHoras/$', views.consultaHoras, name="consultaHoras" ),
    url(r'^consultaEquipos/$', views.consultaEquipos, name="consultaEquipos" ),
    url(r'^prueba/$', views.prueba, name="prueba" ),
]