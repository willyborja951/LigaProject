# Generated by Django 2.0.7 on 2018-08-04 04:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administracion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EncuentroDeportivo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hora', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='FechaDeportiva',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Jugador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('numeroCedula', models.CharField(max_length=10)),
                ('numeroCarnet', models.IntegerField()),
                ('equipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='administracion.Equipo')),
            ],
        ),
        migrations.AddField(
            model_name='encuentrodeportivo',
            name='equipo1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo1', to='administracion.Equipo'),
        ),
        migrations.AddField(
            model_name='encuentrodeportivo',
            name='equipo2',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='equipo2', to='administracion.Equipo'),
        ),
        migrations.AddField(
            model_name='encuentrodeportivo',
            name='fecha',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='administracion.FechaDeportiva'),
        ),
    ]
