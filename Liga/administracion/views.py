from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse
from administracion import forms
from administracion import models
from django.core.exceptions import ValidationError
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login as djLogin, logout
from django.contrib.auth.decorators import login_required
import pandas as pd
import numpy as np
from Liga import settings
import simplejson, json
import datetime
from django.views import generic
from django.views import View
from django.db.models import Q

def jsonDefault(object):
    if isinstance(object, datetime.date):
        return dict(year=object.year, month=object.month, day=object.day)
    elif isinstance(object, datetime.time):
        return dict(hour=object.hour, minutes=object.minute)
    else:
        return object.__dict__
#Index
def index(request):
    return render (request, "frontpage/index.html")

#Consultas
def consultas(request):
    fechaDeportiva = models.FechaDeportiva.objects.get(predefinido=1)
    fechas = models.Fecha.objects.filter(fechaDeportiva=fechaDeportiva)
    listaFechas = []
    for fecha in fechas:
        listaFechas.append(models.EncuentroDeportivo.objects.filter(fecha=fecha))
    return render (request, "frontpage/consultas.html", {'listaFechas':listaFechas})

#Login
def login(request):
    if request.method == 'POST':
        formularioLogin = forms.LoginForm(request.POST, prefix="formularioLogin")
        if formularioLogin.is_valid():
            username = formularioLogin.cleaned_data.get('username')
            password = formularioLogin.cleaned_data.get('password')
            credencial = authenticate(username=username, password=password)
            if credencial is not None:                 
                djLogin(request, credencial)                 
                return HttpResponseRedirect(reverse('administracion:dashboard'))
            else:
                messages.add_message(request, messages.ERROR, "El usuario/password proporcionado no es correcto. Porfavor, verifícalo e inténtalo nuevamente.")
        else:
            messages.add_message(request, messages.ERROR, formularioLogin.errors)
    else:
        formularioLogin = forms.LoginForm(prefix="formularioLogin")
    return render (request, "formularioLogin.html", {'formularioLogin':formularioLogin})

#Cerrar sesión
@login_required(login_url='/login/')
def cerrarSesion(request):     
    logout(request)     
    return redirect('administracion:login')

"""
REGISTRAR NUEVOS USUARIOS
"""
@login_required(login_url='/login/')
def registrarUsuario(request):
    if request.method == 'POST':
        formularioCredencial = forms.CredencialForm(request.POST, prefix="formularioCredencial")
        if formularioCredencial.is_valid():
            username = formularioCredencial.cleaned_data.get('username')
            name = formularioCredencial.cleaned_data.get('name')
            lastname = formularioCredencial.cleaned_data.get('lastname') 
            email = formularioCredencial.cleaned_data.get('email')
            password = formularioCredencial.cleaned_data.get('password')
            tipoUsuario = models.ItemCatalogo.objects.get(nombreItem=formularioCredencial.cleaned_data.get('tipoUsuario'))
            nuevaCredencial = models.Credencial(username = username, name = name, lastname = lastname, email = email, tipoUsuario = tipoUsuario)
            nuevaCredencial.set_password(password)  
            nuevaCredencial.save()
            return HttpResponseRedirect(reverse('administracion:login'))
        else:
            messages.add_message(request, messages.ERROR, formularioCredencial.errors)
    else:
        formularioCredencial = forms.CredencialForm(prefix="formularioCredencial")
    return render (request, "formularioRegistro.html", {'formularioCredencial':formularioCredencial})

@login_required(login_url='/login/')
def dashboard(request):
    return render(request, "dashboard.html")

"""
CATALOGOS
"""
#Crea un campeonato deportivo
def agregarCatalogo(request):
    if request.method == 'POST':
        formularioAgregarCatalogo = forms.AgregarCatalogoForm(request.POST, prefix="formularioAgregarCatalogo")
        if formularioAgregarCatalogo.is_valid():
            nombreCatalogo = formularioAgregarCatalogo.cleaned_data.get('nombreCatalogo').lower()
            descripcionCatalogo = formularioAgregarCatalogo.cleaned_data.get('descripcionCatalogo').lower()
            catalogo = models.Catalogo(nombreCatalogo = nombreCatalogo, descripcionCatalogo = descripcionCatalogo)
            catalogo.save()
            return HttpResponseRedirect(reverse('administracion:agregarCatalogo'))
    else:
        formularioAgregarCatalogo = forms.AgregarCatalogoForm(prefix="formularioAgregarCatalogo")
    formularioActualizarCatalogo = forms.ActualizarCatalogoForm(prefix="formularioActualizarCatalogo")
    catalogosRegistrados = models.Catalogo.objects.filter().order_by('id')
    return render (request, "formularioCatalogos.html", {'formularioAgregarCatalogo':formularioAgregarCatalogo, 'formularioActualizarCatalogo':formularioActualizarCatalogo, 'catalogosRegistrados':catalogosRegistrados})

#Actualiza un campeonato deportivo
def actualizarCatalogo(request):
    if request.method == 'POST':
        formularioActualizarCatalogo = forms.ActualizarCatalogoForm(request.POST, prefix="formularioActualizarCatalogo")
        if formularioActualizarCatalogo.is_valid():
            id = formularioActualizarCatalogo.cleaned_data.get('id')
            nombre = formularioActualizarCatalogo.cleaned_data.get('nombreCatalogo').lower()
            descripcion = formularioActualizarCatalogo.cleaned_data.get('descripcionCatalogo').lower()
            catalogo = models.Catalogo.objects.get(id=id)
            catalogo.nombreCatalogo = nombre
            catalogo.descripcionCatalogo = descripcion
            catalogo.save()
    else:
        formularioActualizarCatalogo = forms.ActualizarCatalogoForm(prefix="formularioActualizarCatalogo")
    formularioAgregarCatalogo = forms.AgregarCatalogoForm(prefix="formularioAgregarCatalogo")
    catalogosRegistrados = models.Catalogo.objects.filter().order_by('id')
    return render (request, "formularioCatalogos.html", {'formularioActualizarCatalogo':formularioActualizarCatalogo, 'formularioAgregarCatalogo':formularioAgregarCatalogo, 'catalogosRegistrados':catalogosRegistrados})

#Elimina un campeonato deportivo
def eliminarCatalogo(request, id):
    catalogo = models.Catalogo.objects.get(id=id)
    catalogo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarCatalogo'))

"""
ITEMS CATALOGO
"""
#Crea un campeonato deportivo
def agregarItemCatalogo(request):
    if request.method == 'POST':
        formularioAgregarItemCatalogo = forms.AgregarItemCatalogoForm(request.POST, prefix="formularioAgregarItemCatalogo")
        if formularioAgregarItemCatalogo.is_valid():
            nombreItem = formularioAgregarItemCatalogo.cleaned_data.get('nombreItem').lower()
            catalogo = formularioAgregarItemCatalogo.cleaned_data.get('catalogo')
            itemCatalogo = models.ItemCatalogo(nombreItem = nombreItem, catalogo = catalogo)
            itemCatalogo.save()
            return HttpResponseRedirect(reverse('administracion:agregarItemCatalogo'))
    else:
        formularioAgregarItemCatalogo = forms.AgregarItemCatalogoForm(prefix="formularioAgregarItemCatalogo")
    formularioActualizarItemCatalogo = forms.ActualizarItemCatalogoForm(prefix="formularioActualizarItemCatalogo")
    itemsCatalogoRegistrados = models.ItemCatalogo.objects.filter().order_by('id')
    return render (request, "formularioItemsCatalogo.html", {'formularioAgregarItemCatalogo':formularioAgregarItemCatalogo, 'formularioActualizarItemCatalogo':formularioActualizarItemCatalogo, 'itemsCatalogoRegistrados':itemsCatalogoRegistrados})

#Actualiza un campeonato deportivo
def actualizarItemCatalogo(request):
    if request.method == 'POST':
        formularioActualizarItemCatalogo = forms.ActualizarItemCatalogoForm(request.POST, prefix="formularioActualizarItemCatalogo")
        if formularioActualizarItemCatalogo.is_valid():
            id = formularioActualizarItemCatalogo.cleaned_data.get('id')
            nombreItem = formularioActualizarItemCatalogo.cleaned_data.get('nombreItem').lower()
            catalogo = formularioActualizarItemCatalogo.cleaned_data.get('catalogo')
            itemCatalogo = models.ItemCatalogo.objects.get(id=id)
            itemCatalogo.nombreItem = nombreItem
            itemCatalogo.catalogo = catalogo
            itemCatalogo.save()
    else:
        formularioActualizarItemCatalogo = forms.ActualizarItemCatalogoForm(prefix="formularioActualizarItemCatalogo")
    formularioAgregarItemCatalogo = forms.AgregarItemCatalogoForm(prefix="formularioAgregarItemCatalogo")
    itemsCatalogoRegistrados = models.ItemCatalogo.objects.filter().order_by('id')
    return render (request, "formularioItemsCatalogo.html", {'formularioActualizarItemCatalogo':formularioActualizarItemCatalogo, 'formularioAgregarItemCatalogo':formularioAgregarItemCatalogo, 'itemsCatalogoRegistrados':itemsCatalogoRegistrados})

#Elimina un campeonato deportivo
def eliminarItemCatalogo(request, id):
    itemCatalogo = models.ItemCatalogo.objects.get(id=id)
    itemCatalogo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarItemCatalogo'))

"""
EQUIPOS
"""
def agregarEquipo(request):
    if request.method == 'POST':
        formularioAgregarEquipo = forms.AgregarEquipoForm(request.POST, prefix="formularioAgregarEquipo")
        if formularioAgregarEquipo.is_valid():
            nombre = formularioAgregarEquipo.cleaned_data.get('nombre').lower()
            modalidad = formularioAgregarEquipo.cleaned_data.get('modalidad')
            equipo = models.Equipo(nombre = nombre, modalidad = modalidad)
            equipo.save()
            return HttpResponseRedirect(reverse('administracion:agregarEquipo'))
    else:
        formularioAgregarEquipo = forms.AgregarEquipoForm(prefix="formularioAgregarEquipo")
    formularioActualizarEquipo = forms.ActualizarEquipoForm(prefix="formularioActualizarEquipo")
    equiposRegistrados = models.Equipo.objects.filter().order_by('id')
    return render (request, "formularioEquipos.html", {'formularioAgregarEquipo':formularioAgregarEquipo, 'formularioActualizarEquipo':formularioActualizarEquipo, 'equiposRegistrados':equiposRegistrados})

#Actualiza un campeonato deportivo
def actualizarEquipo(request):
    if request.method == 'POST':
        formularioActualizarEquipo = forms.ActualizarEquipoForm(request.POST, prefix="formularioActualizarEquipo")
        if formularioActualizarEquipo.is_valid():
            id = formularioActualizarEquipo.cleaned_data.get('id')
            nombre = formularioActualizarEquipo.cleaned_data.get('nombre').lower()
            modalidad = formularioActualizarEquipo.cleaned_data.get('modalidad')
            equipo = models.Equipo.objects.get(id=id)
            equipo.nombre = nombre
            equipo.modalidad = modalidad
            equipo.save()
            return HttpResponseRedirect(reverse('administracion:agregarEquipo'))
    else:
        formularioActualizarEquipo = forms.ActualizarEquipoForm(prefix="formularioActualizarEquipo")
    formularioAgregarEquipo = forms.AgregarEquipoForm(prefix="formularioAgregarEquipo")
    equiposRegistrados = models.Equipo.objects.filter().order_by('id')
    return render (request, "formularioEquipos.html", {'formularioActualizarEquipo':formularioActualizarEquipo, 'formularioAgregarEquipo':formularioAgregarEquipo, 'equiposRegistrados':equiposRegistrados})

#Elimina un campeonato deportivo
def eliminarEquipo(request, id):
    equipo = models.Equipo.objects.get(id=id)
    equipo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarEquipo'))

"""
CAMPEONATOS DEPORTIVOS
"""
#Crea un campeonato deportivo
def agregarCampeonatoDeportivo(request):
    if request.method == 'POST':
        formularioAgregarCampeonatoDeportivo = forms.AgregarCampeonatoDeportivoForm(request.POST, prefix="formularioAgregarCampeonatoDeportivo")
        if formularioAgregarCampeonatoDeportivo.is_valid():
            nombre = formularioAgregarCampeonatoDeportivo.cleaned_data.get('nombre').lower()
            fechaInicio = formularioAgregarCampeonatoDeportivo.cleaned_data.get('fechaInicio')
            predefinido = formularioAgregarCampeonatoDeportivo.cleaned_data.get('predefinido')
            if predefinido == True:
                cambiarCampeonatosPredefinidos()
            campeonatoDeportivo = models.CampeonatoDeportivo(nombre = nombre, fechaInicio = fechaInicio, predefinido=predefinido)
            campeonatoDeportivo.save()
            return HttpResponseRedirect(reverse('administracion:agregarCampeonatoDeportivo'))
    else:
        formularioAgregarCampeonatoDeportivo = forms.AgregarCampeonatoDeportivoForm(prefix="formularioAgregarCampeonatoDeportivo")
    formularioActualizarCampeonatoDeportivo = forms.ActualizarCampeonatoDeportivoForm(prefix="formularioActualizarCampeonatoDeportivo")
    campeonatosDeportivosRegistrados = models.CampeonatoDeportivo.objects.filter().order_by('id')
    return render (request, "formularioCampeonatosDeportivos.html", {'formularioAgregarCampeonatoDeportivo':formularioAgregarCampeonatoDeportivo, 'formularioActualizarCampeonatoDeportivo':formularioActualizarCampeonatoDeportivo, 'campeonatosDeportivosRegistrados':campeonatosDeportivosRegistrados})

#Actualiza un campeonato deportivo
def actualizarCampeonatoDeportivo(request):
    if request.method == 'POST':
        formularioActualizarCampeonatoDeportivo = forms.ActualizarCampeonatoDeportivoForm(request.POST, prefix="formularioActualizarCampeonatoDeportivo")
        if formularioActualizarCampeonatoDeportivo.is_valid():
            id = formularioActualizarCampeonatoDeportivo.cleaned_data.get('id')
            nombre = formularioActualizarCampeonatoDeportivo.cleaned_data.get('nombre').lower()
            fechaInicio = formularioActualizarCampeonatoDeportivo.cleaned_data.get('fechaInicio')
            predefinido = formularioActualizarCampeonatoDeportivo.cleaned_data.get('predefinido')
            campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(id=id)
            campeonatoDeportivo.nombre = nombre
            campeonatoDeportivo.fechaInicio = fechaInicio
            campeonatoDeportivo.predefinido = predefinido
            if predefinido == True:
                cambiarCampeonatosPredefinidos()
            campeonatoDeportivo.save()
            return HttpResponseRedirect(reverse('administracion:agregarCampeonatoDeportivo'))
    else:
        formularioActualizarCampeonatoDeportivo = forms.ActualizarCampeonatoDeportivoForm(request.POST, prefix="formularioActualizarCampeonatoDeportivo")
    campeonatosDeportivosRegistrados = models.CampeonatoDeportivo.objects.filter().order_by('id')
    formularioAgregarCampeonatoDeportivo = forms.AgregarCampeonatoDeportivoForm(prefix="formularioAgregarCampeonatoDeportivo")
    return render (request, "formularioCampeonatosDeportivos.html", {'formularioActualizarCampeonatoDeportivo':formularioActualizarCampeonatoDeportivo, 'formularioAgregarCampeonatoDeportivo':formularioAgregarCampeonatoDeportivo, 'listaCampeonatosDeportivos':campeonatosDeportivosRegistrados})

#Elimina un campeonato deportivo
def eliminarCampeonatoDeportivo(request, id):
    campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(id=id)
    campeonatoDeportivo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarCampeonatoDeportivo'))

def cambiarCampeonatosPredefinidos():
    try:
        campeonatosDeportivosPredefinidos = models.CampeonatoDeportivo.objects.filter(predefinido=True)
        for campeonatoDeportivo in campeonatosDeportivosPredefinidos:
            campeonatoDeportivo.predefinido = False
            campeonatoDeportivo.save()
    except:
        pass

"""
SUBCAMPEONATO
"""
#Crea un campeonato deportivo
def agregarSubcampeonato(request):
    if request.method == 'POST':
        formularioAgregarSubcampeonato = forms.AgregarSubcampeonatoForm(request.POST, prefix="formularioAgregarSubcampeonato")
        if formularioAgregarSubcampeonato.is_valid():
            campeonatoDeportivo = formularioAgregarSubcampeonato.cleaned_data.get('campeonatoDeportivo')
            modalidad = formularioAgregarSubcampeonato.cleaned_data.get('modalidad')
            subcampeonato = models.Subcampeonato(campeonatoDeportivo = campeonatoDeportivo, modalidad=modalidad)
            subcampeonato.save()
            return HttpResponseRedirect(reverse('administracion:agregarSubcampeonato'))
    else:
        formularioAgregarSubcampeonato = forms.AgregarSubcampeonatoForm(prefix="formularioAgregarSubcampeonato")
    formularioActualizarSubcampeonato = forms.ActualizarSubcampeonatoForm(prefix="formularioActualizarSubcampeonato")
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter().order_by('id')
    return render (request, "formularioSubcampeonatos.html", {'formularioAgregarSubcampeonato':formularioAgregarSubcampeonato, 'formularioActualizarSubcampeonato':formularioActualizarSubcampeonato, 'subcampeonatosRegistrados':subcampeonatosRegistrados})

#Actualiza un campeonato deportivo
def actualizarSubcampeonato(request):
    if request.method == 'POST':
        formularioActualizarSubcampeonato = forms.ActualizarSubcampeonatoForm(request.POST, prefix="formularioActualizarSubcampeonato")
        if formularioActualizarSubcampeonato.is_valid():
            id = formularioActualizarSubcampeonato.cleaned_data.get('id')
            campeonatoDeportivo = formularioActualizarSubcampeonato.cleaned_data.get('campeonatoDeportivo')
            modalidad = formularioActualizarSubcampeonato.cleaned_data.get('modalidad')
            subcampeonato = models.Subcampeonato.objects.get(id=id)
            subcampeonato.campeonatoDeportivo = campeonatoDeportivo
            subcampeonato.modalidad = modalidad
            subcampeonato.save()
            return HttpResponseRedirect(reverse('administracion:agregarSubcampeonato'))
    else:
        formularioActualizarSubcampeonato = forms.ActualizarSubcampeonatoForm(request.POST, prefix="formularioActualizarSubcampeonato")
    formularioAgregarSubcampeonato = forms.AgregarSubcampeonatoForm(prefix="formularioAgregarSubcampeonato")
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter().order_by('id')
    return render (request, "formularioSubcampeonatos.html", {'formularioAgregarSubcampeonato':formularioAgregarSubcampeonato, 'formularioActualizarSubcampeonato':formularioActualizarSubcampeonato, 'subcampeonatosRegistrados':subcampeonatosRegistrados})

#Elimina un campeonato deportivo
def eliminarSubcampeonato(request, id):
    subcampeonato = models.Subcampeonato.objects.get(id=id)
    subcampeonato.delete()
    return HttpResponseRedirect(reverse('administracion:agregarSubcampeonato'))

"""
INSCRIPCION EQUIPO
"""
def agregarInscripcionEquipo(request):
    if request.method == 'POST':
        formularioAgregarInscripcionEquipo = forms.AgregarInscripcionEquipoForm(request.POST, prefix="formularioAgregarInscripcionEquipo")
        if formularioAgregarInscripcionEquipo.is_valid():
            subcampeonato = formularioAgregarInscripcionEquipo.cleaned_data.get('subcampeonato')
            equipo = formularioAgregarInscripcionEquipo.cleaned_data.get('equipo')
            inscripcionEquipo = models.InscripcionEquipo(subcampeonato=subcampeonato, equipo=equipo)
            inscripcionEquipo.save()
            return HttpResponseRedirect(reverse('administracion:agregarInscripcionEquipo'))
    else:
        formularioAgregarInscripcionEquipo = forms.AgregarInscripcionEquipoForm(prefix="formularioAgregarInscripcionEquipo")
    campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter(campeonatoDeportivo=campeonatoDeportivo)
    inscripcionesEquipoRegistradas = models.InscripcionEquipo.objects.filter(subcampeonato_id__in=subcampeonatosRegistrados).order_by('-subcampeonato')
    return render (request, "formularioInscripcionesEquipo.html", {'formularioAgregarInscripcionEquipo':formularioAgregarInscripcionEquipo, 'inscripcionesEquipoRegistradas':inscripcionesEquipoRegistradas})

def consultaEquiposSubcampeonato(request):
    idSubcampeonato = json.loads(request.POST['idSubcampeonato'])
    subcampeonato = models.Subcampeonato.objects.get(id=idSubcampeonato)
    equipos = models.Equipo.objects.filter(modalidad=subcampeonato.modalidad)
    listaEquipos = {}
    i=0
    for i in range (len(equipos)):
        listaEquipos[i]={'idEquipo':equipos[i].id, 'equipo':equipos[i].nombre}
        i+=1
    listaEquiposJson = json.dumps(listaEquipos, default=jsonDefault)
    return HttpResponse(json.dumps(listaEquiposJson, default=jsonDefault), content_type='application/json')

def consultaSubcampeonatosActuales(request):
    campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter(campeonatoDeportivo=campeonatoDeportivo)
    listaSubcampeonatos = {}
    i=0
    for i in range (len(subcampeonatosRegistrados)):
        listaSubcampeonatos[i]={'id':subcampeonatosRegistrados[i].id, 'nombre':subcampeonatosRegistrados[i].modalidad.nombreItem}
        i+=1
    listaSubcampeonatosJson = json.dumps(listaSubcampeonatos, default=jsonDefault)
    return HttpResponse(json.dumps(listaSubcampeonatosJson, default=jsonDefault), content_type='application/json')

#Elimina una inscripción equipo
def eliminarInscripcionEquipo(request, id):
    inscripcionEquipo = models.InscripcionEquipo.objects.get(id=id)
    inscripcionEquipo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarInscripcionEquipo'))

"""
JUGADOR
"""
#Crea un campeonato deportivo
def agregarJugador(request):
    if request.method == 'POST':
        formularioAgregarJugador = forms.AgregarJugadorForm(request.POST, prefix="formularioAgregarJugador")
        if formularioAgregarJugador.is_valid():
            nombres = formularioAgregarJugador.cleaned_data.get('nombres').lower()
            apellidos = formularioAgregarJugador.cleaned_data.get('apellidos').lower()
            numeroCedula = formularioAgregarJugador.cleaned_data.get('numeroCedula')
            genero = formularioAgregarJugador.cleaned_data.get('genero')
            jugador = models.Jugador(nombres = nombres, apellidos = apellidos, numeroCedula = numeroCedula, genero=genero)
            jugador.save()
            return HttpResponseRedirect(reverse('administracion:agregarJugador'))
    else:
        formularioAgregarJugador = forms.AgregarJugadorForm(prefix="formularioAgregarJugador")
    formularioActualizarJugador = forms.ActualizarJugadorForm(prefix="formularioActualizarJugador")
    jugadoresRegistrados = models.Jugador.objects.filter().order_by('id')
    return render (request, "formularioJugadores.html", {'formularioAgregarJugador':formularioAgregarJugador, 'formularioActualizarJugador':formularioActualizarJugador, 'jugadoresRegistrados':jugadoresRegistrados})

#Actualiza un campeonato deportivo
def actualizarJugador(request):
    if request.method == 'POST':
        formularioActualizarJugador = forms.ActualizarJugadorForm(request.POST, prefix="formularioActualizarJugador")
        if formularioActualizarJugador.is_valid():
            id = formularioActualizarJugador.cleaned_data.get('id')
            nombres = formularioActualizarJugador.cleaned_data.get('nombres').lower()
            apellidos = formularioActualizarJugador.cleaned_data.get('apellidos').lower()
            numeroCedula = formularioActualizarJugador.cleaned_data.get('numeroCedula')
            genero = formularioActualizarJugador.cleaned_data.get('genero')
            jugador = models.Jugador.objects.get(id=id)
            jugador.nombres = nombres
            jugador.apellidos = apellidos
            jugador.numeroCedula = numeroCedula
            jugador.genero = genero
            jugador.save()
            return HttpResponseRedirect(reverse('administracion:agregarJugador'))
    else:
        formularioActualizarJugador = forms.ActualizarJugadorForm(request.POST, prefix="formularioActualizarJugador")
    formularioAgregarJugador = forms.AgregarJugadorForm(prefix="formularioAgregarJugador")
    jugadoresRegistrados = models.Jugador.objects.filter().order_by('id')
    return render (request, "formularioJugadores.html", {'formularioAgregarJugador':formularioAgregarJugador, 'formularioActualizarJugador':formularioActualizarJugador, 'jugadoresRegistrados':jugadoresRegistrados})

#Elimina un campeonato deportivo
def eliminarJugador(request, id):
    jugador = models.Jugador.objects.get(id=id)
    jugador.delete()
    return HttpResponseRedirect(reverse('administracion:agregarJugador'))

"""
PASE DEPORTIVO
"""
#Crea un campeonato deportivo
def agregarPaseDeportivo(request):
    if request.method == 'POST':
        formularioAgregarPaseDeportivo = forms.AgregarPaseDeportivoForm(request.POST, prefix="formularioAgregarPaseDeportivo")
        if formularioAgregarPaseDeportivo.is_valid():
            jugador = formularioAgregarPaseDeportivo.cleaned_data.get('jugador')
            equipoDestino = formularioAgregarPaseDeportivo.cleaned_data.get('equipoDestino')
            campeonatoDeportivoVigencia = models.CampeonatoDeportivo.objects.get(predefinido=True)
            paseDeportivo = models.PaseDeportivo(jugador = jugador, equipoDestino = equipoDestino, campeonatoDeportivoVigencia = campeonatoDeportivoVigencia)
            paseDeportivo.save()
            return HttpResponseRedirect(reverse('administracion:agregarPaseDeportivo'))
    else:
        formularioAgregarPaseDeportivo = forms.AgregarPaseDeportivoForm(prefix="formularioAgregarPaseDeportivo")
    campeonatoActual = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter(campeonatoDeportivo=campeonatoActual)
    gruposJugadores = []
    for campeonato in subcampeonatosRegistrados:
        gruposJugadores.append(models.Jugador.objects.filter(genero=models.ItemCatalogo.objects.get(nombreItem=campeonato.modalidad.nombreItem, catalogo=models.Catalogo.objects.get(nombreCatalogo="genero de jugadores"))))
    pasesDeportivosRegistrados = models.PaseDeportivo.objects.filter().order_by('id')
    return render (request, "formularioPasesDeportivos.html", {'formularioAgregarPaseDeportivo':formularioAgregarPaseDeportivo, 'pasesDeportivosRegistrados':pasesDeportivosRegistrados, 'gruposJugadores':gruposJugadores})

#Elimina un campeonato deportivo
def eliminarPaseDeportivo(request, id):
    paseDeportivo = models.PaseDeportivo.objects.get(id=id)
    paseDeportivo.delete()
    return HttpResponseRedirect(reverse('administracion:agregarPaseDeportivo'))

def consultarEquiposInscritosEnCampeonatoDeportivo(request):
    idJugador = json.loads(request.POST['idJugador'])
    jugador = models.Jugador.objects.get(id=idJugador)
    campeonatoActual = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatoPermitido = models.Subcampeonato.objects.get(campeonatoDeportivo=campeonatoActual, modalidad=models.ItemCatalogo.objects.get(catalogo=models.Catalogo.objects.get(nombreCatalogo="modalidad de campeonato"), nombreItem=jugador.genero.nombreItem))
    inscripcionesEquipos = models.InscripcionEquipo.objects.filter(subcampeonato=subcampeonatoPermitido)
    listaEquiposParticipantes = {}
    i=0
    for i in range (len(inscripcionesEquipos)):
        listaEquiposParticipantes[i]={'id':inscripcionesEquipos[i].equipo.id, 'nombre':inscripcionesEquipos[i].equipo.nombre}
        i+=1
    listaEquiposParticipantesJson = json.dumps(listaEquiposParticipantes, default=jsonDefault)
    return HttpResponse(json.dumps(listaEquiposParticipantesJson, default=jsonDefault), content_type='application/json')

"""
INSCRIPCION JUGADOR
"""
#Crea un campeonato deportivo
def agregarInscripcionJugador(request):
    if request.method == 'POST':
        formularioAgregarInscripcionJugador = forms.AgregarInscripcionJugadorForm(request.POST, prefix="formularioAgregarInscripcionJugador")
        if formularioAgregarInscripcionJugador.is_valid():
            listaJugadores = request.POST.getlist('formularioAgregarInscripcionJugador-jugador')
            inscripcionEquipo = formularioAgregarInscripcionJugador.cleaned_data.get('inscripcionEquipo')
            for idJugador in listaJugadores:
                jugador = models.Jugador.objects.get(id=idJugador)
                jugador.equipo = inscripcionEquipo.equipo
                jugador.save()
                inscripcionJugador = models.InscripcionJugador(jugador = jugador, inscripcionEquipo = inscripcionEquipo)
                inscripcionJugador.save()
            return HttpResponseRedirect(reverse('administracion:agregarInscripcionJugador'))
    else:
        formularioAgregarInscripcionJugador = forms.AgregarInscripcionJugadorForm(prefix="formularioAgregarInscripcionJugador")
    campeonatoActual = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter(campeonatoDeportivo=campeonatoActual)
    return render (request, "formularioInscripcionesJugadores.html", {'formularioAgregarInscripcionJugador':formularioAgregarInscripcionJugador, 'campeonatoActual':campeonatoActual, 'subcampeonatosRegistrados':subcampeonatosRegistrados})

#Elimina un campeonato deportivo
def eliminarInscripcionJugador(request, id):
    inscripcionJugador = models.InscripcionJugador.objects.get(id=id)
    inscripcionJugador.delete()
    return HttpResponseRedirect(reverse('administracion:agregarInscripcionJugador'))

def consultarEquiposInscritosDadoSubcampeonato(request):
    idSubcampeonato = json.loads(request.POST['idSubcampeonato'])
    subcampeonato = models.Subcampeonato.objects.get(id=idSubcampeonato)
    inscripcionesEquipos = models.InscripcionEquipo.objects.filter(subcampeonato=subcampeonato)
    listaEquipos = {}
    i=0
    for i in range (len(inscripcionesEquipos)):
        listaEquipos[i]={'id':inscripcionesEquipos[i].id, 'nombre':inscripcionesEquipos[i].equipo.nombre}
        i+=1
    listaEquiposJson = json.dumps(listaEquipos, default=jsonDefault)
    return HttpResponse(json.dumps(listaEquiposJson, default=jsonDefault), content_type='application/json')

def consultarJugadoresDadoSubcampeonato(request):
    idSubcampeonato = json.loads(request.POST['idSubcampeonato'])
    idInscripcionEquipo = json.loads(request.POST['idInscripcionEquipo'])
    inscripcionesEquipoOrdenadas = models.InscripcionEquipo.objects.filter(equipo=models.InscripcionEquipo.objects.get(id=idInscripcionEquipo).equipo).order_by('id')
    jugadoresAnteriorCampeonato = {}
    jugadoresSinAsignarEquipo = {}
    jugadoresExtraEquipo = {}
    i=0

    jugadoresExtra = models.Jugador.objects.filter(equipo_id = models.InscripcionEquipo.objects.get(id=idInscripcionEquipo).equipo.id)


    #Obtiene todos los jugadores que participaron dentro de un equipo, durante el anterior campeonato
    if (len(inscripcionesEquipoOrdenadas)>1):
        j=0
        for j in range (len(inscripcionesEquipoOrdenadas)):
            if inscripcionesEquipoOrdenadas[j].id == idInscripcionEquipo and j>0:
                inscripcionesJugadores = models.InscripcionJugador.objects.filter(inscripcionEquipo=inscripcionesEquipoOrdenadas[j-1])
                for i in range (len(inscripcionesJugadores)):
                    jugadoresAnteriorCampeonato[i]={'id':inscripcionesJugadores[i].jugador.id, 'numeroCedula':inscripcionesJugadores[i].jugador.numeroCedula, 'genero':inscripcionesJugadores[i].jugador.genero.nombreItem, 'nombres':inscripcionesJugadores[i].jugador.nombres, 'apellidos':inscripcionesJugadores[i].jugador.apellidos }
                    jugadoresExtra = jugadoresExtra.exclude(id=inscripcionesJugadores[i].jugador.id)
                    i+=1
                break
            j+=1

    #Otros jugadores inscritos
    i=0
    for i in range (len(jugadoresExtra)):
        jugadoresExtraEquipo[i]={'id':jugadoresExtra[i].id, 'numeroCedula':jugadoresExtra[i].numeroCedula, 'genero':jugadoresExtra[i].genero.nombreItem, 'nombres':jugadoresExtra[i].nombres, 'apellidos':jugadoresExtra[i].apellidos }
        i+=1
            
    #Obtiene todos los jugadores que no han sido inscritos en un club deportivo aun
    subcampeonato = models.Subcampeonato.objects.get(id=idSubcampeonato)
    jugadoresSinEquipo = models.Jugador.objects.filter(equipo__isnull=True,  genero=models.ItemCatalogo.objects.get(catalogo=models.Catalogo.objects.get(nombreCatalogo="genero de jugadores"), nombreItem=subcampeonato.modalidad))
    i=0
    for jugadorSinEquipo in jugadoresSinEquipo:
        jugadoresSinAsignarEquipo[i]={'id':jugadorSinEquipo.id, 'numeroCedula':jugadorSinEquipo.numeroCedula, 'genero':jugadorSinEquipo.genero.nombreItem, 'nombres':jugadorSinEquipo.nombres, 'apellidos':jugadorSinEquipo.apellidos }
        i+=1 

    listaJugadores = {'jugadoresAnteriorCampeonato':[jugadoresAnteriorCampeonato], 'jugadoresSinAsignarEquipo':[jugadoresSinAsignarEquipo], 'jugadoresExtraEquipo':[jugadoresExtraEquipo]}       

    listaJugadoresJson = json.dumps(listaJugadores, default=jsonDefault)
    return HttpResponse(json.dumps(listaJugadoresJson, default=jsonDefault), content_type='application/json')


    """
    subcampeonato = models.Subcampeonato.objects.get(id=idSubcampeonato)
    jugadores = models.Jugador.objects.filter(genero=models.ItemCatalogo.objects.get(catalogo=models.Catalogo.objects.get(nombreCatalogo="genero de jugadores"), nombreItem=subcampeonato.modalidad))
    """










'''
ENCUENTROS DEPORTIVOS
'''
def agregarEncuentrosDeportivos(request):
    if request.method == 'POST':
        formularioAgregarEncuentrosDeportivos = forms.EncuentroDeportivoForm(request.POST, prefix="formularioAgregarEncuentrosDeportivos")
        if formularioAgregarEncuentrosDeportivos.is_valid():
            fechaIngresada = formularioAgregarEncuentrosDeportivos.cleaned_data.get('fecha')
            fechaRegistrada = models.Fecha.objects.get(fecha=datetime.date(fechaIngresada.fecha.year, fechaIngresada.fecha.month, fechaIngresada.fecha.day))
            listaHoras = request.POST.getlist('formularioAgregarEncuentrosDeportivos-hora')
            listaEquipos1 = request.POST.getlist('formularioAgregarEncuentrosDeportivos-equipo1')
            listaEquipos2 = request.POST.getlist('formularioAgregarEncuentrosDeportivos-equipo2')
            
            for l in range (len(listaHoras)):
                hora = listaHoras[l]
                idEquipo1 = listaEquipos1[l]
                equipo1Registrado = models.Equipo.objects.get(id=idEquipo1)
                idEquipo2 = listaEquipos2[l]
                equipo2Registrado = models.Equipo.objects.get(id=idEquipo2)
                encuentroDeportivo = models.EncuentroDeportivo(fecha=fechaRegistrada, hora=hora, equipo1=equipo1Registrado, equipo2=equipo2Registrado)
                encuentroDeportivo.save()
        else:
            messages.add_message(request, messages.ERROR, formularioAgregarEncuentrosDeportivos.errors)
    else:
        formularioAgregarEncuentrosDeportivos = forms.EncuentroDeportivoForm(prefix="formularioAgregarEncuentrosDeportivos")
    campeonatoDeportivo = models.CampeonatoDeportivo.objects.get(predefinido=True)
    subcampeonatosRegistrados = models.Subcampeonato.objects.filter(campeonatoDeportivo=campeonatoDeportivo).order_by('id')
    horariosRegistrados = models.Horario.objects.filter().order_by('id')
    return render (request, "formularioEncuentrosDeportivos.html", {'formularioAgregarEncuentrosDeportivos':formularioAgregarEncuentrosDeportivos, 'subcampeonatosRegistrados':subcampeonatosRegistrados, 'listaHorarios':horariosRegistrados})










@login_required(login_url='/login/')
def subirListado(request):
    if request.method == 'POST':
        formularioListaEquipos = forms.ListaEquipos(request.POST, request.FILES, prefix="formularioListaEquipos")
        if formularioListaEquipos.is_valid():
            formularioListaEquipos.save()
            file = request.FILES['formularioListaEquipos-file']
            dirFile = settings.MEDIA_URL+file.name
            df = pd.read_csv(dirFile)
            df = df.fillna("")

            for equipo in list(df['EQUIPO'].unique()):
                print("entro")
                equipo = models.Equipo(nombre=equipo)
                equipo.save()

            for index, row in df.iterrows():
                print("nueva evaluacion")
                strEquipo = row["EQUIPO"]
                strNombres = row["NOMBRES"]
                strApellidos = row["APELLIDOS"]
                strCedula = row["NO. CEDULA"]
                intCarnet = row["CARNET"]

                if (strEquipo != "" and strNombres != "" and strApellidos != "" 
                and strCedula != "" and intCarnet != ""):
                    if(existeCedula(strCedula)==False):
                        if(existeNumeroCarnet(strEquipo, intCarnet)==False):
                            equipo = models.Equipo.objects.get(nombre=strEquipo)
                            jugador = models.Jugador(equipo = equipo, nombres = strNombres, 
                            apellidos = strApellidos, numeroCedula = strCedula, numeroCarnet = intCarnet)
                            print(jugador)
                            jugador.save()
                        else:
                            jugadorFallido = models.JugadorFallido(equipo = equipo, nombres = strNombres, 
                            apellidos = strApellidos, numeroCedula = strCedula, numeroCarnet = intCarnet,
                            observacion = "El número de carnet ingresado ya existe en el equipo")
                            jugadorFallido.save()
                    else:
                        jugadorFallido = models.JugadorFallido(equipo = equipo, nombres = strNombres, 
                        apellidos = strApellidos, numeroCedula = strCedula, numeroCarnet = intCarnet,
                        observacion = "El número de cédula ingresado ya existe")
                        jugadorFallido.save()
                
                else:
                    jugadorFallido = models.JugadorFallido(equipo = equipo, nombres = strNombres, 
                    apellidos = strApellidos, numeroCedula = strCedula, numeroCarnet = intCarnet,
                    observacion = "Existe un campo que no se ha llenado")
                    jugadorFallido.save()

            listas = models.ListaEquipos.objects.all()
            listas.delete()
            messages.add_message(request, messages.SUCCESS, "Se ha guardado la información de los equipos y deportistas satisfacoriamente")
        else:
            messages.add_message(request, messages.ERROR, formularioListaEquipos.errors)
    else:
        formularioListaEquipos = forms.ListaEquipos(prefix="formularioListaEquipos")
    return render (request, "formularioListaEquipos.html", {'formularioListaEquipos':formularioListaEquipos})

def existeCedula(strCedula):
    try:
        jugador = models.Jugador.objects.get(numeroCedula=strCedula)
        return True
    except:
        return False

def existeNumeroCarnet(strEquipo, intCarnet):
    equipo = models.Equipo.objects.get(nombre=strEquipo)
    try:
        jugador = models.Jugador.objects.get(numeroCarnet=intCarnet)
    except:
        return False

@login_required(login_url='/login/')
def administrarFechasDeportivas(request):
    if request.method == 'POST':
        formularioAdministrarFechasDeportivas = forms.FechaDeportivaForm(request.POST, prefix="formularioAdministrarFechasDeportivas")
        if formularioAdministrarFechasDeportivas.is_valid():
            nombre = formularioAdministrarFechasDeportivas.cleaned_data.get('nombre').upper()
            fecha = models.FechaDeportiva(nombre=nombre)
            fecha.save()
        else:
            messages.add_message(request, messages.ERROR, formularioAdministrarFechasDeportivas.errors)
    else:
        formularioAdministrarFechasDeportivas = forms.FechaDeportivaForm(prefix="formularioAdministrarFechasDeportivas")
    fechasRegistradas = models.FechaDeportiva.objects.filter().order_by('id')
    return render (request, "formularioAdministrarFechasDeportivas.html", {'formularioAdministrarFechasDeportivas':formularioAdministrarFechasDeportivas, 'listaFechas':fechasRegistradas})

def eliminarFechaDeportiva(request, idFecha):
    fecha = models.FechaDeportiva.objects.get(id=idFecha)
    fecha.delete()
    return HttpResponseRedirect(reverse('administracion:administrarFechasDeportivas'))

def actualizarFechaDeportiva(request):
    if request.method == 'POST':
        formularioActualizarFechaDeportiva = forms.ActualizarFechaDeportivaForm(request.POST, prefix="formularioActualizarFechaDeportiva")
        if formularioActualizarFechaDeportiva.is_valid():
            if existeNombreFechaDeportiva == False:
                idFecha = formularioActualizarFechaDeportiva.cleaned_data.get('idNombre')
                nombreFecha = formularioActualizarFechaDeportiva.cleaned_data.get('nombre').upper()
                fecha = models.FechaDeportiva.objects.get(id=idFecha)
                fecha.nombre = nombreFecha
                fecha.save()
            else:
                messages.add_message(request, messages.ERROR, "El nombre de la fecha deportiva ingresada, ya existe. Porfavor, cámbialo.")    
        else:
            messages.add_message(request, messages.ERROR, formularioActualizarFechaDeportiva.errors)
    return HttpResponseRedirect(reverse('administracion:administrarFechasDeportivas'))

def existeNombreFechaDeportiva(nombre):
    try:
        fechaDeportiva = models.FechaDeportiva.objects.get(nombre=nombre)
        return True
    except:
        return False

def administrarHorarios(request):
    if request.method == 'POST':
        formularioAdministrarHorarios = forms.HorarioForm(request.POST, prefix="formularioAdministrarHorarios")
        if formularioAdministrarHorarios.is_valid():
            nombre = formularioAdministrarHorarios.cleaned_data.get('nombre').upper()
            fecha = models.Horario(nombre=nombre)
            fecha.save()
        else:
            messages.add_message(request, messages.ERROR, formularioAdministrarHorarios.errors)
    else:
        formularioAdministrarHorarios = forms.HorarioForm(prefix="formularioAdministrarHorarios")
    horariosRegistrados = models.Horario.objects.filter().order_by('id')
    return render (request, "formularioAdministrarHorarios.html", {'formularioAdministrarHorarios':formularioAdministrarHorarios, 'listaHorarios':horariosRegistrados})

def actualizarHorario(request):
    if request.method == 'POST':
        formularioActualizarHorario = forms.ActualizarHorarioForm(request.POST, prefix="formularioActualizarHorario")
        if formularioActualizarHorario.is_valid():
            nombre = formularioActualizarHorario.cleaned_data.get('nombre').upper()
            if existeNombreHorario(nombre) == False:
                idHorario = formularioActualizarHorario.cleaned_data.get('idHorario')
                horario = models.Horario.objects.get(id=idHorario)
                horario.nombre = nombre
                horario.save()
            else:
                messages.add_message(request, messages.ERROR, "El nombre del horario ingresado, ya existe. Porfavor, cámbialo.")    
        else:
            messages.add_message(request, messages.ERROR, formularioActualizarHorario.errors)
    return HttpResponseRedirect(reverse('administracion:administrarHorarios'))

def existeNombreHorario(nombre):
    try:
        fechaDeportiva = models.Horario.objects.get(nombre=nombre)
        return True
    except:
        return False

def eliminarHorario(request, idHorario):
    horario = models.Horario.objects.get(id=idHorario)
    horario.delete()
    return HttpResponseRedirect(reverse('administracion:administrarHorarios'))

def administrarFechas(request):
    if request.method == 'POST':
        formularioAdministrarFechas = forms.FechaForm(request.POST, prefix="formularioAdministrarFechas")
        if formularioAdministrarFechas.is_valid():
            nombreFechaDeportiva = formularioAdministrarFechas.cleaned_data.get('fechaDeportiva')
            fechaDeportiva = models.FechaDeportiva.objects.get(nombre=nombreFechaDeportiva)
            fechaAsignada = formularioAdministrarFechas.cleaned_data.get('fecha')
            fecha = models.Fecha(fechaDeportiva=fechaDeportiva, fecha=fechaAsignada)
            fecha.save()
            print(fecha)
        else:
            messages.add_message(request, messages.ERROR, formularioAdministrarFechas.errors)
    else:
        print("holas")
        formularioAdministrarFechas = forms.FechaForm(prefix="formularioAdministrarFechas")
    fechasDeportivasRegistradas = models.FechaDeportiva.objects.filter().order_by('id')
    fechasRegistradas = models.Fecha.objects.filter().order_by('id')
    return render (request, "formularioAdministrarFechas.html", {'formularioAdministrarFechas':formularioAdministrarFechas, 'listaFechas':fechasRegistradas, 'listaFechasDeportivas':fechasDeportivasRegistradas})

def actualizarFecha(request):
    if request.method == 'POST':
        formularioActualizarFecha = forms.ActualizarFechaForm(request.POST, prefix="formularioActualizarFecha")
        if formularioActualizarFecha.is_valid():
            idFecha = formularioActualizarFecha.cleaned_data.get('idFecha')
            fechaDeportiva = formularioActualizarFecha.cleaned_data.get('fechaDeportiva')
            fechaAsignada = formularioActualizarFecha.cleaned_data.get('fecha')
            fecha = models.Fecha.objects.get(id=idFecha)
            fecha.fecha = fechaAsignada
            fecha.fechaDeportiva = fechaDeportiva
            fecha.save()
        else:
            print(formularioActualizarFecha.errors)
            messages.add_message(request, messages.ERROR, formularioActualizarFecha.errors)
    return HttpResponseRedirect(reverse('administracion:administrarFechas'))

def eliminarFecha(request, idFecha):
    fecha = models.Fecha.objects.get(id=idFecha)
    fecha.delete()
    return HttpResponseRedirect(reverse('administracion:administrarFechas'))

def administrarHoras(request):
    if request.method == 'POST':
        formularioAdministrarHoras = forms.HoraForm(request.POST, prefix="formularioAdministrarHoras")
        if formularioAdministrarHoras.is_valid():
            nombreHorarioIngresado = formularioAdministrarHoras.cleaned_data.get('horario')
            horario = models.Horario.objects.get(nombre=nombreHorarioIngresado)
            horaAsignada = formularioAdministrarHoras.cleaned_data.get('hora')
            hora = models.Hora(horario=horario, hora=horaAsignada)
            hora.save()
        else:
            messages.add_message(request, messages.ERROR, formularioAdministrarHoras.errors)
    else:
        formularioAdministrarHoras = forms.HoraForm(prefix="formularioAdministrarHoras")
    horariosRegistrados = models.Horario.objects.filter().order_by('id')
    horasRegistradas = models.Hora.objects.filter().order_by('id')
    return render (request, "formularioAdministrarHoras.html", {'formularioAdministrarHoras':formularioAdministrarHoras, 'listaHorarios':horariosRegistrados, 'listaHoras':horasRegistradas})

def actualizarHora(request):
    if request.method == 'POST':
        formularioActualizarHora = forms.ActualizarHoraForm(request.POST, prefix="formularioActualizarHora")
        if formularioActualizarHora.is_valid():
            idHora = formularioActualizarHora.cleaned_data.get('idHora')
            horarioAsignado = formularioActualizarHora.cleaned_data.get('horario')
            horario = models.Horario.objects.get(nombre=horarioAsignado)
            horaAsignada = formularioActualizarHora.cleaned_data.get('hora')
            hora = models.Hora.objects.get(id=idHora)
            hora.horario = horarioAsignado
            hora.hora = horaAsignada
            hora.save()
        else:
            print(formularioActualizarHora.errors)
            messages.add_message(request, messages.ERROR, formularioActualizarHora.errors)
    return HttpResponseRedirect(reverse('administracion:administrarHoras'))

def eliminarHora(request, idHora):
    hora = models.Hora.objects.get(id=idHora)
    hora.delete()
    return HttpResponseRedirect(reverse('administracion:administrarHoras'))

def consultaHoras(request):
    idHorario = json.loads(request.POST['idHorario'])
    horario = models.Horario.objects.get(id=idHorario)
    horas = models.Hora.objects.filter(horario=horario)
    listaHoras = {}
    i=0
    for i in range (len(horas)):
        listaHoras[i]={'idHora':horas[i].id, 'hora':horas[i]}
        i+=1
    listaHorasJson = json.dumps(listaHoras, default=jsonDefault)
    return HttpResponse(json.dumps(listaHorasJson, default=jsonDefault), content_type='application/json')
   
def consultaEquipos(request):
    equipos = models.Equipo.objects.all()
    listaEquipos = {}
    i=0
    for i in range (len(equipos)):
        listaEquipos[i]={'idEquipo':equipos[i].id, 'equipo':equipos[i]}
        i+=1
    listaEquiposJson = json.dumps(listaEquipos, default=jsonDefault)
    return HttpResponse(json.dumps(listaEquiposJson, default=jsonDefault), content_type='application/json')

def prueba(request):
    idFechaDeportiva = json.loads(request.POST['dato'])
    fechaDeportiva = models.FechaDeportiva.objects.get(id=idFechaDeportiva)
    fechas = models.Fecha.objects.filter(fechaDeportiva=fechaDeportiva)
    listaFechas = {}
    i=0
    for i in range (len(fechas)):
        listaFechas[i]={'idFecha':fechas[i].id, 'fecha':fechas[i]}
        i+=1
    listaFechasJson = json.dumps(listaFechas, default=jsonDefault)
    return HttpResponse(json.dumps(listaFechasJson, default=jsonDefault), content_type='application/json')
